# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  lc = ("a".."z")
  str.each_char.select{|char| !lc.include?(char)}.join
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  if str.length.odd?
    return str.chars[str.length/2]
  else
    return str.chars[(str.length / 2)-1] + str.chars[(str.length / 2)]
  end
end

# Return the number of vowels in a string.
vowels = %w(a e i o u)
def num_vowels(str)
  vowels = %w(a e i o u)
  ctr = 0
  str.each_char{|char| ctr += 1 if vowels.include?(char)}
  ctr
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  (1..num).inject(:*)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  master = ""
  arr.each_with_index do |i, idx|
    master += i
    if idx < arr.length - 1
      master += separator
    end
  end
  master
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  (0..str.length - 1).each do |idx|
    if idx.even?
      str[idx] = str[idx].downcase
    else
      str[idx] = str[idx].upcase
    end
  end
  str
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  master = []
  str.split.each do |word|
    if word.length > 4
      master << word.reverse
    else
      master << word
    end
  end
  master.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  master = []
  (1..n).each do |i|
    if i % 3 == 0 && i % 5 == 0
      master << "fizzbuzz"
    elsif i % 3 == 0 && !(i % 5 == 0)
      master << "fizz"
    elsif !(i % 3 == 0) && i % 5 == 0
      master << "buzz"
    else
      master << i
    end
  end
  master

end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  master = []
  arr.each do |i|
    master.unshift(i)
  end
  master
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  if num == 1
    return false
  end

  (2...num).each do |i|
    if num % i == 0
      return false
    end
  end
  return true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  master = []
  (1..num).each do |i|
    if num % i == 0
      master << i
    end
  end
  master.sort
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  arr = factors(num)
  arr.select {|i| prime?(i)}
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).size
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  odds = arr.select{|i| i.odd?}
  if odds.size > 1
    evens = arr.select{|j| j.even?}
    return evens[0]
  end

  return odds[0]

end
